# Aethlabs microAeth MA series driver

Python 3 module for interacting with Aethlabs microAeth MA series Black carbon monitors via serial connection

## Features
Read all available csv formats

### TODO
Measure command  
Device settings commands

## Usage

```python
from microaeth_ma_controller import microAeth_MA

# Data request
ma = microAeth_MA('/dev/ttyUSB0')
data = ma.data_request_cmd()

print(data)
```
