import yaml
import serial
import csv
import os
import re
import time

class microAeth_MA:
    def __init__(self, port):
        self.csv_formats={}
        self.menu_tree={}
        self.csv_format = ()
        self.port = port
        self.sampling = False
        # Get serial formats
        with open(os.path.dirname(__file__)+'/csv_format.yaml', mode='r') as file:
            self.csv_formats = yaml.load(file, Loader=yaml.FullLoader)
        with open(os.path.dirname(__file__)+'/menu.yaml', mode='r') as file:
            self.menu_tree = yaml.load(file, Loader=yaml.FullLoader)
    def detect_format(self, data, csv_formats):
        # Detect format
        data_format = ('','')
        if len(data) == len(csv_formats['minimal']['SingleSpot']):
            data_format = ('minimal', 'SingleSpot');
        else:
            if len(data) == len(csv_formats['minimal']['DualSpot']):
                data_format = ('minimal', 'DualSpot');
            else:
                for verbose_format_item in csv_formats['verbose'].keys():
                    if verbose_format_item in data:
                        data_format = ('verbose', verbose_format_item);
        print("Format: " + data_format[0] + ' ' + data_format[1])
        return data_format

    def floatify_list(self, list):
        # Make numbers float
        for idx,item in enumerate(list):
            try:
                list[idx]=(float(item))
            except:
                pass
        return list

    def data_request_cmd(self):
        # request measurement data
        with serial.Serial(self.port, 1000000, timeout=3) as ser:
            ser.write(b"dr\r")
            ser.read(3)
            line = ser.readline()
            if len(line) > 0:
                csv_data = self.floatify_list(line.decode().replace('\n', '').replace('\r', '').split(','))
                data_format = self.detect_format(csv_data, self.csv_formats)
                return dict(zip(self.csv_formats[data_format[0]][data_format[1]], csv_data))
            else:
                return []

    def get_status_cmd(self):
        status = False
        with serial.Serial(self.port, 1000000, timeout=3) as ser:
            ser.write(b"cs\r")
            ser.readline()
            status = re.sub(r"[\r\n ]", "", ser.readline().decode()).split(',')
            for idx,item in enumerate(status):
                status[idx] = item.split('=')
                status[idx][1] = int(status[idx][1])
            status = dict(status)
            self.sampling = status['sampling'] == 1
        return status

    def menu_navigate(self, menu_item):
        success = False
        menu_position=''
        cntr=0
        with serial.Serial(self.port, 1000000, timeout=3) as ser:
            ser.reset_input_buffer()
            while menu_position != self.menu_tree[menu_item]['name'] \
            and cntr <= len(self.menu_tree):
                ser.write(b"ur\r\n")
                line = re.sub(r"[\r\n]", "",ser.readline().decode()).split(':')
                if len(line) == 1:
                    return False
                menu_position = line[1].strip()
                # print(menu_position, self.menu_tree[menu_item]['name'])
                cntr = cntr + 1
                time.sleep(0.2)
            ser.write(b"uc\r\n")
        return menu_position == self.menu_tree[menu_item]['name']

    def menu_set_value(self, menu_item, setting = False):
        if setting != False:
            # if 'values' in self.menu_tree[menu_item].keys():
            #     if setting not in self.menu_tree[menu_item]['values']:
            #         return False
            print("Setting " + menu_item + ":" + str(setting))
        if self.menu_navigate(menu_item) == False:
            return False
        if setting == False:
            return True
        time.sleep(0.2)
        cntr=0
        direction = 'r'
        menu_position = ''
        if 'max' in self.menu_tree[menu_item].keys():
            limit = self.menu_tree[menu_item]['max'] - self.menu_tree[menu_item]['min'] + 1
        else:
            limit = len(self.menu_tree[menu_item]['values'])+1
        print("Limit: ", limit)
        if isinstance(setting, str):
            return False
        with serial.Serial(self.port, 1000000, timeout=3) as ser:
            ser.reset_input_buffer()
            while menu_position != setting and cntr <= limit:
                if direction == 'r':
                    ser.write(b"ur\r\n")
                else:
                    ser.write(b"ul\r\n")
                line = re.sub(r"[\r\n]", "",ser.readline().decode()).split(':')
                menu_position = int(line[1].strip())
                if len(line) == 1:
                    return False
                if menu_position > setting:
                    direction = 'l'
                print(menu_position, setting)
                cntr = cntr + 1
                time.sleep(0.2)
            ser.write(b"uc\r\n")
        return menu_position == setting
